import * as Brand from 'https://unpkg.com/@devoinc/genesys-brand-devo@5.10.0/dist/index.esm.js';

const AVAILABLE_THEMES = { ...Brand, none: {} };

const innerTheme = {
  tokens: AVAILABLE_THEMES.none
};

const themeHandler = {
  get(target, prop, receiver) {
    if (time.time.value < 1000 && 'v5' === prop || time.time.value >= 1000 && 'v6' === prop) {
      // Case 1: what is expected (all good):
      stage.className = '';
      return target;
    } else if (time.time.value < 1000 && 'v4' === prop || 'v5' === prop) {
      // Case 2: one version behind (warning):
      stage.className = 'warning';
      return target;
    } else if (time.time.value >= 1000 && time.time.value < 2000 && 'v4' === prop) {
      // Case 3: two versions behind during a transition period (alert):
      stage.className = 'alert';
      return target;
    } else {
      // Case 4: accessing an invalid version (error):
      destroyCurrentSatellite();
      stage.className = 'alert';
      // window.alert('[Message shown only on development environment]\nSatellite tried to access inexistent theme');
      throw new Error('Satellite tried to access inexistent theme');
    }
  }
};

const theme = new Proxy(innerTheme, themeHandler);

const cssURL = option =>
  'none' !== option ? `https://unpkg.com/@devoinc/genesys-brand-devo/dist/${option}/css/tokens.css.alias.css` : '';

let currentSatellite, stage, settings, resetLink, themeLink, time; // , timeLabel;

const toggleTheme = () => {
  const option = settings.theme.value;
  themeLink.href = cssURL(option);
  if (settings.reset.checked && 'stylesheet' !== resetLink.rel) {
    resetLink.rel = 'stylesheet';
    document.body.className = 'tiny';
  } else if (!settings.reset.checked && 'stylesheet' === resetLink.rel) {
    resetLink.rel = 'none';
    document.body.className = '';
  }
  theme.tokens = AVAILABLE_THEMES[option];
  stage.className = '';
  if (currentSatellite)
    currentSatellite.init(stage, theme);
};

const moveInTime = () => {
  if (time.time.value < 1000)
    timeLabel.innerHTML = `<p>Before the upgrade:</p>
<pre>theme {
    v4: 🍏,
    <strong>v5: 🍍</strong>
}</pre>`;
  else if (time.time.value < 2000)
    timeLabel.innerHTML = `<p>During the transition period:</p>
<pre>theme {
    v4: 🍏,
    v5: 🍍,
    <strong>v6: 🍇</strong>
}</pre>`;
  else
    timeLabel.innerHTML = `<p>After the upgrade:</p>
<pre>theme {
    v5: 🍍,
    <strong>v6: 🍇</strong>
}</pre>`;
};

const setTime = () => {
  window.accessTheme = time.using.value;
  toggleTheme();
};

const destroyCurrentSatellite = () => {
  stage.innerHTML = '';
  if (currentSatellite) {
    currentSatellite.destroy();
    currentSatellite = null;
  }
};

const goHome = () => {
  console.debug('Go home');
  stage.innerHTML = '<h2>Welcome</h2>';
};

const loadSatellite = id => {
  console.debug('Load satellite “' + id + '”');
  import(`./satellites/${id}.mjs`)
    .then(module => {
      stage.className = '';
      currentSatellite = module.default;
      currentSatellite.init(stage, theme);
    })
    .catch(console.error);
};

const init = () => {
  if (!location.hash ||
    '#' === document.URL.charAt(document.URL.length - 1))
    location.replace('#home');
  stage = document.getElementsByTagName('main')[0];
  settings = document.getElementById('themeMenu');
  settings.addEventListener('change', toggleTheme);
  resetLink = document.getElementById('resetLink');
  themeLink = document.getElementById('themeLink');
  time = document.getElementById('timeMenu');
  // time = document.getElementById('time');
  // timeLabel = document.getElementById('timeLabel');
  time.addEventListener('input', moveInTime);
  time.addEventListener('change', setTime);
  moveInTime();
  const entries = menu.getElementsByTagName('a');
  for (const entry of entries)
    entry.addEventListener('click', event => {
      event.preventDefault();
      destroyCurrentSatellite();
      const target = entry.hash;
      location.replace(target);
      if ('#home' === target)
        goHome();
      else
        loadSatellite(target.substring(1));
    });
  if ('#home' !== location.hash)
    loadSatellite(location.hash.substring(1));
  else
    goHome();
};

window.addEventListener('load', init, { once: true });
