import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

export default defineConfig({
  base: './',
  plugins: [react()],
  optimizeDeps: {
    exclude: [],
  },
  server: {
    port: 4000,
  },
  logLevel: 'info',
  build: {
    sourcemap: true,
    minify: false
  }
});
