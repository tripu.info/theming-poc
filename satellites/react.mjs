import * as React from 'https://unpkg.com/react@18/umd/react.development.js';
import * as ReactDOM from 'https://unpkg.com/react-dom@18/umd/react-dom.development.js';
import Counter from './counter.mjs';

const content = `
<h2>React</h2>
<p>
  A <strong>JS module</strong> that creates a <strong>React root</strong>,
  loads an <strong>external React component</strong> <code>Counter</code>
  and instantiates it passing a number that increases each time this satellite is loaded.
</p>
<p>
  As before, both this module and the component module are typically <strong>cached</strong> by the browser,
  so <strong>subsequent instantiations are quick</strong>.
</p>
 <div id="canvas" class="react" />
`;

let times = 0;

const innerInit = stage => {
  console.debug('react: init');
  stage.innerHTML = content;
  const canvas = document.getElementById('canvas');
  const root = this.ReactDOM.createRoot(canvas);
  root.render(this.React.createElement(Counter, { count: ++times }));
};

const init = innerInit.bind({React, ReactDOM});

const destroy = () => {
  console.debug('react: destroy');
};

export default { init, destroy };
