class Counter extends React.Component {

  render() {
    return React.createElement('p', null, `Instantiated ${this.props.count} times`);
  }

}

export default Counter;
