const app_metadata = { app: 'hello-world', team: 'foo squad', version: '1' };


const content = `
<h2>Hello, world!</h2>
<p>A <strong>single native JS module</strong> with embedded HTML.</p>
`;

const init = stage => {
  console.debug('hello-world: init');
  stage.innerHTML = content;
  return app_metadata;
};

const destroy = () => {
  console.debug('hello-world: destroy');
};

export default { init, destroy };
