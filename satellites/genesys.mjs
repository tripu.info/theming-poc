import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './app.mjs';

const content = `
<h2>Genesys</h2>
<p>
  A <strong>JS module</strong> that creates a <strong>React root</strong> and
  loads an <strong>external React component</strong> <code>App</code>
  which contains <strong>Genesys UI</strong> components.
</p>
<p>
  Again, all loaded dynamically and <strong>cached</strong> client-side.
</p>
<p>
  <strong>NB:</strong> this satellite fails if there is no schema, since it relies on Genesys themes.
</p>
<div id="canvas" />
`;

const init = (stage, themes) => {
  const theme = themes['v' + window.accessTheme];
  console.debug('genesys: init');
  stage.innerHTML = content;
  const canvas = document.getElementById('canvas');
  const root = ReactDOM.createRoot(canvas);
  root.render(
    React.createElement(React.StrictMode, null,
      React.createElement(App, { theme })
    )
  );
};

const destroy = () => {
  console.debug('genesys: destroy');
};

export default { init, destroy };
