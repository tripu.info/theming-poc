import { Component, createElement } from 'react';
import { ThemeProvider } from 'styled-components';
import { Panel, Typography } from '@devoinc/genesys-ui';

class App extends Component {

  render() {
    return createElement(ThemeProvider, { theme: this.props.theme },
      createElement(Panel, { title: 'genesys UI app', subtitle: 'Just a demo' },
        createElement(Typography.Paragraph, null,
          'Lorem ipsum! '.repeat(20)
        )
      )
    );
  }

}

export default App;
