let active = false;
let content, now;

const showTime = () => {
  if (active) {
    now.innerText = new Date().toLocaleTimeString();
    setTimeout(showTime, 1000);
  }
}

const init = stage => {
  console.debug('vanilla: init');
  if (!content) {
    fetch(`assets/vanilla.html`)
      .then(response => response.text())
      .then(html => {
        content = html;
        stage.innerHTML = content;
        now = document.getElementById('now');
        active = true;
        setTimeout(showTime, 1000);
      })
      .catch(console.error);
  } else {
    stage.innerHTML = content;
    now = document.getElementById('now');
    active = true;
    setTimeout(showTime, 1000);
  }
};

const destroy = () => {
  console.debug('vanilla: destroy');
  active = false;
};

export default { init, destroy };
