_Proof of concept for a modular application with theme switching_

## The idea

This is a web application made up of two parts: a **lightweight &ldquo;shell&rdquo;** including navigation,
and a **set of &ldquo;satellites&rdquo;** or &ldquo;modules&rdquo; that are loaded dynamically when the user
navigates to each one of them.

The look &amp; feel (**the &ldquo;theme&rdquo;**) is entirely the responsibility of the shell: it exposes
an object with all necessary information about the theme, and the satellites use that to style themselves.

We want to test this idea both with simple HTML+JS satellites (theming with plain CSS),
and with **satellites using React and [Genesys UI](https://github.com/DevoInc/genesys-ui) components**.

## Goals

1. Pure front-end: no devDependencies, build step, SSR or transpilation of any kind
   (to keep requirements to a minimum, and focus only on the concept we want to test)
1. Satellites should be a native JS module (ESM) conforming to a minimalistic interface
   (see details below)
1. Satellites are entirely independent and free to load any resources or assets they may need
1. Satellites have to be loaded entirely on demand (so that the app stays nimble and fast)
1. Switching the theme (at the level of the shell) must cause immediate change to the look &amp; feel
   of both the shell itself and whatever satellite(s) is/are on display at that time

## How to run and develop

It's just a static site: **simply serve the root directory and visit `index.html`**.

For development, you may want to use Node.js, npm and
[the package `light-server`](https://www.npmjs.com/package/light-server),
```sh
npx light-server -s . -w '*, **/*'
```
which comes with hot-reloading.
The shell script `start.sh` is shorthand for that.
```sh
./start.sh
```

To update the [architecture diagram](architecture.png), make sure you have
[Graphviz](https://www.graphviz.org/) (eg, `sudo apt-get install graphviz`),
edit the file [`architecture.gv`](architecture.gv), and then issue this command:

```shell
dot architecture.gv -Tpng -o architecture.png
```

## In detail

![Architecture diagram](architecture.png)

```
.
├── index.html
├── style.css
├── behaviour.mjs
└── satellites
    ├── foo.mjs
    ├── bar.mjs
    ├── bar.html
    ├── baz.mjs
    ├── h2.mjs
    └── quux.mjs
```

Regarding the shell&ndash;satellite interface, a satellite exports (at least) these two functions:

* `init(stage)`: instantiate the satellite;
   the required parameter is an existing [Element](https://developer.mozilla.org/en-US/docs/Web/API/Element)
   in the DOM underneath which the satellite can inject its content (destroying whatever were there previously)
* `destroy()`: remove the satellite, freeing any resources it may have used
